-module(exor).
-export([test/0, xor0/2, xor1/2, xor2/2, xor3/2, xor4/2]).

%% Supplied function.
xor0(true, false) ->
    true;
xor0(false, true) ->
    true;
xor0(_, _) ->
    false.

%% Supplied function.
xor1(_X, _X) ->
    false;
xor1(_, _) ->
    true.

xor2(X, Y) ->
    not (X == Y).

xor3(X, Y) ->
    X =/= Y.

xor4(X, Y) ->
    (X orelse Y) andalso not (X andalso Y).

test() ->
    io:fwrite("Testing…~n"),
    test([fun xor0/2, fun xor1/2, fun xor2/2, fun xor3/2, fun xor4/2], 0).

test([], _) ->
    io:fwrite("Done.~n");
test([F|Fs], N) ->
    io:fwrite("xor~B", [N]),
    T1 = F(true, true),   % Expect 'false'.
    T2 = F(true, false),  % Expect 'true'.
    T3 = F(false, true),  % Expect 'true'.
    T4 = F(false, false), % Expect 'false'.
    if not (not T1 andalso T2 andalso T3 andalso not T4) ->
            io:fwrite(" failed.~n"
                      ++ "    t, t: ~s~n"
                      ++ "    t, f: ~s~n"
                      ++ "    f, t: ~s~n"
                      ++ "    f, f: ~s~n~n",
                      [T1, T2, T3, T4]);
       true ->
            io:fwrite(" passed.~n"),
            test(Fs, N+1)
    end.
