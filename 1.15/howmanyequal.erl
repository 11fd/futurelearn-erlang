-module(howmanyequal).
-export([test/0, howManyEqual/3]).

howManyEqual(A, A, A) ->
    3;
howManyEqual(A, A, _) ->
    2;
howManyEqual(A, _, A) ->
    2;
howManyEqual(_, B, B) ->
    2;
howManyEqual(_, _, _) ->
    0.

test() ->
    io:fwrite("Testing…~n"),
    [A, B, C] = [34, 25, 36],
    assert(A, B, C, 0),
    assert(A, B, A, 2),
    assert(A, A, A, 3),
    io:fwrite("Done.~n").

assert(A, B, C, Expected) ->
    R = howManyEqual(A, B, C),
    io:fwrite("(~B, ~B, ~B) gives ~B: ", [A, B, C, R]),
    if Expected == R ->
            io:fwrite("passed.~n");
       true ->
            io:fwrite("expected ~B.~n", [Expected])
    end.
