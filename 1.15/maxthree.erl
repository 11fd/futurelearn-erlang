-module(maxthree).
-export([test/0, maxThree/3]).

maxThree(A, B, C) ->
    max(A, max(B, C)).

test() ->
    io:fwrite("Testing…~n"),
    [A, B, C] = [34, 25, 36],
    assert(A, B, C, C),
    assert(B, C, A, C),
    assert(B, B, B, B),
    assert(A, B, B, A),
    io:fwrite("Done.~n").

assert(A, B, C, Expected) ->
    R = maxThree(A, B, C),
    io:fwrite("(~B, ~B, ~B) gives ~B: ", [A, B, C, R]),
    if Expected == R ->
            io:fwrite("passed.~n");
       true ->
            io:fwrite("expected ~B.~n", [Expected])
    end.
