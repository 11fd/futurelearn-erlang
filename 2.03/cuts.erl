-module(cuts).
-export([pieces/1,
         pieces3d/1,
         pieces_nth_d/2]).

pieces(0) ->
    1;
pieces(N) when N > 0 ->
    N + pieces(N - 1).

%% Shan't lie; had to look this one up:
%% https://en.wikipedia.org/wiki/Cake_number
pieces3d(0) ->
    1;
pieces3d(N) when N > 0 ->
    pieces(N - 1) + pieces3d(N - 1).

%% Surprisingly easy, though no-doubt inefficient.
pieces_nth_d(0, D) when D > 0 ->
    %% When you don't cut, you don't increase pieces.
    1;
pieces_nth_d(N, 0) when N > 0 ->
    %% When in 0D you only have a single un-cuttable point.
    1;
pieces_nth_d(N, D) when N > 0, D > 0 ->
    pieces_nth_d(N - 1, D - 1) + pieces_nth_d(N - 1, D).
