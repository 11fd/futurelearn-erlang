-module(tail_perfect).
-export([perfect/1,
         test/0,
         sperfect/1]).

perfect(N) ->
    perfect(N, N div 2, 0).

perfect(N, 0, N) when N > 0 ->
    true;
perfect(_, 0, _) ->
    false;
perfect(N, _, _) when N =< 0 orelse N rem 2 =/= 0 ->
    false;
perfect(N, Div, Acc) when N rem Div == 0 ->
    perfect(N, Div - 1, Div + Acc);
perfect(N, Div, Acc) ->
    perfect(N, Div - 1, Acc).

test() ->
    false = perfect(4),
    true = perfect(6),
    false = perfect(10),
    true = perfect(28),
    false = perfect(160),
    true = perfect(496),
    false = perfect(599),
    true = perfect(8128),
    false = perfect(12345678),
    true = perfect(33550336),
    ok.

%% Simon's Solution
sperfect(N) ->
    sperfect(N, 1, 0).

sperfect(N, N, S) ->
    N == S;
sperfect(N, M, S) when N rem M == 0 ->
    sperfect(N, M + 1, S + M);
sperfect(N, M, S) ->
    sperfect(N, M + 1, S).
