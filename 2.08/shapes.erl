-module(shapes).
-export([perimeter/1,
         area/1,
         enclose/1]).

%% Use {atom, …} identfiers as discussed in lesson 1.11
perimeter({square, S}) ->
    4*S;
perimeter({rectangle, S1, S2}) ->
    2*S1 + 2*S2;
perimeter({circle, Rad}) ->
    2*Rad * math:pi();
perimeter({triangle, S1, S2, S3}) ->
    S1 + S2 + S3.

area({square, S}) ->
    S*S;
area({rectangle, S1, S2}) ->
    S1*S2;
area({circle, Rad}) ->
    Rad*Rad * math:pi();
area({triangle, S1, S2, S3} = T) ->
    %% Heron's formula.
    S = perimeter(T)/2,
    math:sqrt(S*(S - S1)*(S - S2)*(S - S3)).

enclose({square, S}) ->
    {rectangle, S, S};
enclose({rectangle, _, _} = R) ->
    R;
enclose({circle, Rad}) ->
    {rectangle, Rad, Rad};
enclose({triangle, S1, S2, S3} = T) ->
    %% Area = ½ × Base × Height
    Base = max(S1, max(S2, S3)),
    Area = area(T),
    Height = 2 * Area / Base,
    {rectangle, Base, Height}.
