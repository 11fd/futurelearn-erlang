-module(constructing_lists).
-include_lib("eunit/include/eunit.hrl").
-export([double/1,
         direct_double/1,
         evens/1,
         direct_evens/1,
         median/1,
         modes/1]).

%% Emulate lists:map/2.
double(L) ->
    double(L, []).
double([], Acc) ->
    lists:reverse(Acc);
double([X|Xs], Acc) ->
    double(Xs, [2*X|Acc]).

%% Direct-recursion variant of above.
direct_double([]) ->
    [];
direct_double([X|Xs]) ->
    [2*X|direct_double(Xs)].

%% Emulate lists:filter/2.
evens(L) ->
    lists:reverse(evens(L, [])).
evens([], Acc) ->
    Acc;
evens([X|Xs], Acc) ->
    case X rem 2 of
        0 ->
            evens(Xs, [X|Acc]);
        _ ->
            evens(Xs, Acc)
    end.

%% Direct-recursion variant of above.
direct_evens([]) ->
    [];
direct_evens([X|Xs]) ->
    case X rem 2 of
        0 ->
            [X|direct_evens(Xs)];
        _ ->
            direct_evens(Xs)
    end.

%% Emulate length/1.
len(L) ->
    len(L, 0).
len([], Acc) ->
    Acc;
len([_|Rest], Acc) ->
    len(Rest, 1+Acc).

%% Direct-recursion variant of above.
direct_len([]) ->
    0;
direct_len([_|Rest]) ->
    1 + direct_len(Rest).

%% Emulate lists:sort/2 naively, inefficiently.
quick_sort_rev([]) ->
    [];
quick_sort_rev([P|Rest]) ->
    quick_sort_rev([E || E <- Rest, E >= P ])
        ++ [P]
        ++ quick_sort_rev([E || E <- Rest, E < P]).

%% Could use lists:nth/2 to avoid recursion.
median([_|_] = L) ->
    Len = len(L),
    median(quick_sort_rev(L), Len div 2, Len rem 2 =:= 0).
median([H|_], 0, false) ->
    H;
median([H, N|_], 1, true) ->
    (H + N) / 2;
median([_|T], Len, LenEven) ->
    median(T, Len - 1, LenEven).

%% Could use lists:mapfoldl/3 to accumulate lists while counting the run.
%% On a sorted list, track the length of runs of the same value; compare
%% to the maximum run-length at the end of the run
modes([_|_] = L) ->
    [H|T] = quick_sort_rev(L),
    modes(T, H, 1, 1, []).
modes([], X, RunLen, MaxRunLen, Acc) ->
    %% Is there a way that avoids two if's?
    if RunLen < MaxRunLen -> Acc;
       RunLen > MaxRunLen -> [X];
       RunLen =:= MaxRunLen -> [X|Acc]
    end;
modes([X|Rest], X, RunLen, MaxRunLen, Acc) ->
    modes(Rest, X, 1+RunLen, MaxRunLen, Acc);
modes([Y|Rest], X, RunLen, MaxRunLen, Acc) ->
    %% Not mode if run was smaller than max.
    %% Becomes only mode is run was larger.
    %% New mode if run was same.
    if RunLen < MaxRunLen -> modes(Rest, Y, 1, MaxRunLen, Acc);
       RunLen > MaxRunLen -> modes(Rest, Y, 1, RunLen, [X]);
       RunLen =:= MaxRunLen -> modes(Rest, Y, 1, RunLen, [X|Acc])
    end.

double_test() ->
    ?assertEqual([], double([])),
    ?assertEqual([2], double([1])),
    ?assertEqual([2, 4, 6], double([1, 2, 3])),
    ?assertEqual([], direct_double([])),
    ?assertEqual([2], direct_double([1])),
    ?assertEqual([2, 4, 6], direct_double([1, 2, 3])).

evens_test() ->
    ?assertEqual([], evens([])),
    ?assertEqual([], evens([1, 3, 5])),
    ?assertEqual([2, 4, 6], evens([2, 4, 6])),
    ?assertEqual([0, 2, 4], evens([0, 1, 2, 3, 4])),
    ?assertEqual([], direct_evens([])),
    ?assertEqual([], direct_evens([1, 3, 5])),
    ?assertEqual([2, 4, 6], direct_evens([2, 4, 6])),
    ?assertEqual([0, 2, 4], direct_evens([0, 1, 2, 3, 4])).

len_test() ->
    ?assertEqual(0, len([])),
    ?assertEqual(1, len([0])),
    ?assertEqual(4, len([0,1,2,3])),
    ?assertEqual(0, direct_len([])),
    ?assertEqual(1, direct_len([0])),
    ?assertEqual(4, direct_len([0,1,2,3])).

quick_sort_rev_test() ->
    ?assertEqual([], quick_sort_rev([])),
    ?assertEqual([0], quick_sort_rev([0])),
    ?assertEqual([0, 0, 0], quick_sort_rev([0, 0, 0])),
    ?assertEqual([3, 2, 1], quick_sort_rev([1, 2, 3])),
    ?assertEqual([3, 2, 1], quick_sort_rev([3, 2, 1])).

median_test() ->
    ?assertError(function_clause, median([])),
    ?assertEqual(0, median([0])),
    ?assertEqual(0.5, median([0, 1])),
    ?assertEqual(1, median([0, 1, 2])).

modes_test() ->
    ?assertError(function_clause, modes([])),
    ?assertEqual([0], modes([0])),
    ?assertEqual([0], modes([0, 0, 0, 0])),
    ?assertEqual([0, 1, 2, 3], modes([0, 1, 2, 3])),
    ?assertEqual([0, 1], modes([0, 0, 1, 1])),
    ?assertEqual([0], modes([0, 1, 0, 2, 2, 0, 3, 3, 3, 0 ])),
    ?assertEqual([3], modes([3, 0, 3, 1, 1, 3, 2, 2, 2, 3 ])).
