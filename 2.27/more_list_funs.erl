-module(more_list_funs).
-include_lib("eunit/include/eunit.hrl").
-compile(export_all).
-export([join/2,
         direct_join/2,
         concat/1,
         direct_concat/1,
         member/2,
         td_merge_sort/1,
         bu_merge_sort/1,
         quick_sort/1,
         insertion_sort/1]).


%% Join two lists into one, preserving order.
-spec join([T], [T]) -> [T].
join(Xs, Ys) ->
    join(Xs, Ys, []).
join([], [], Acc) ->
    lists:reverse(Acc);
join([], [Y|Ys], Acc) ->
    join([], Ys, [Y|Acc]);
join([X|Xs], Ys, Acc) ->
    join(Xs, Ys, [X|Acc]).

%% Direct-recursion variant of join/2.
-spec direct_join([T], [T]) -> [T].
direct_join([], Ys) ->
    Ys;
direct_join([X|Xs], Ys) ->
    [X|direct_join(Xs, Ys)].


%% Join a nested list into one list.
%% Effectively, flatten.
-spec concat([]) -> [];
            ([[T]]) -> [T].
concat(L) ->
    concat(L, []).
concat([], Acc) ->
    Acc;
concat([X|Xs], Acc) ->
    concat(Xs, join(Acc, X)).

%% Direct-recursion variation of concat/1
-spec direct_concat([]) -> [];
                   ([[T]]) -> [T].
direct_concat([]) ->
    [];
direct_concat([X|Xs]) ->
    direct_join(X, direct_concat(Xs)).


%% Check whether a list contains a certain element.
-spec member(integer(), [_]) -> true | false;
            (integer(), []) -> false.
member(_,[]) ->
    false;
member(X, [X|_]) ->
    true;
member(X, [_|Ys]) ->
    member(X, Ys).


%% Top-down merge sort: recursively split and then merge list.
-spec td_merge_sort([T]) -> [T].
td_merge_sort([]) ->
    [];
td_merge_sort([_]=Xs) ->
    Xs;
td_merge_sort(Xs) ->
    {L,R} = td_split(Xs),
    td_merge(td_merge_sort(L), td_merge_sort(R)).

%% Split a list into two equal (or almost-equal) lists.
-spec td_split([T]) -> {[T], [T]}.
td_split(L) ->
    td_split(L, [], []).
td_split([], L, R) ->
    {L, R};
td_split([X], L, R) ->
    {L, [X|R]};
td_split([X, Y|Tail], L, R) ->
    td_split(Tail, [X|L], [Y|R]).

%% Tail-recursively merge two lists into one.
-spec td_merge([T], [T]) -> [T].
td_merge(Xs, Ys) ->
    lists:reverse(td_merge(Xs, Ys, [])).
td_merge([], [], Acc) ->
    Acc;
td_merge([X|Xs], [], Acc) ->
    td_merge(Xs, [], [X|Acc]);
td_merge([], [Y|Ys], Acc) ->
    td_merge([], Ys, [Y|Acc]);
td_merge([X|Xs], [Y|_] = Ys, Acc) when X =< Y ->
    td_merge(Xs, Ys, [X|Acc]);
td_merge([_|_] = Xs, [Y|Ys], Acc) ->
    td_merge(Xs, Ys, [Y|Acc]).


%% Bottom-up merge sort: split a list into a list of one-element
%% lists, and then recursively merge each consecutive pair.
-spec bu_merge_sort([T]) -> [T].
bu_merge_sort([]) ->
    [];
bu_merge_sort([_] = L) ->
    L;
bu_merge_sort(L) ->
    bu_merge_sort(bu_split(L), []).
bu_merge_sort([], [End]) ->
    End;
bu_merge_sort([], Acc) ->
    bu_merge_sort(Acc, []);
bu_merge_sort([X], [Y|Acc]) ->
    bu_merge_sort([], [bu_merge(X, Y)|Acc]);
bu_merge_sort([X, Y|L], Acc) ->
    bu_merge_sort(L, [bu_merge(X, Y)|Acc]).

%% Split a list into a list of single-element lists.
-spec bu_split([T]) -> [[T]].
bu_split(L) ->
    bu_split(L,[]).
bu_split([], Acc) ->
    Acc;
bu_split([X|Xs], Acc) ->
    bu_split(Xs, [[X]|Acc]).

%% Merge two ordered lists into one.
-spec bu_merge([T], [T]) -> [T].
bu_merge([], []) ->
    [];
bu_merge([X|Xs], []) ->
    [X|bu_merge(Xs, [])];
bu_merge([], [Y|Ys]) ->
    [Y|bu_merge([], Ys)];
bu_merge([X|Xs] = Xa, [Y|Ys] = Ya) ->
    if X =< Y ->
            [X|bu_merge(Xs, Ya)];
       true ->
            [Y|bu_merge(Xa, Ys)]
    end.


%% Quicksort implementation.
-spec quick_sort([T]) -> [T].
quick_sort([]) ->
    [];
quick_sort([_] = L) ->
    L;
quick_sort([H|T]) ->
    {St, Eq, Gt} = partition(H, T),
    concat([quick_sort(St), [H|Eq], quick_sort(Gt)]).

%% Group list elements into three groups depending on pivot
%% relation.
-spec partition(T, [T]) -> {[T], [T], [T]}.
partition(P, L) ->
    partition(P, L, [], [], []).
partition(_, [], St, Eq, Gt) ->
    {St, Eq, Gt};
partition(P, [X|Xs], St, Eq, Gt) ->
    if P < X ->
            partition(P, Xs, St, Eq, [X|Gt]);
       P > X ->
            partition(P, Xs, [X|St], Eq, Gt);
       P == X ->
            partition(P, Xs, St, [X|Eq], Gt)
    end.


%% Tail-recursive implementation of insert-sort.
-spec insertion_sort([T]) -> [T].
insertion_sort(L) ->
    insertion_sort(L, []).
insertion_sort([], Acc) ->
    Acc;
insertion_sort([X|Xs], Acc) ->
    insertion_sort(Xs, insert(X, Acc)).

%% Insert an element into the correct place in the list.
-spec insert(T, [T]) -> [T].
insert(N, []) ->
    [N];
insert(N, [X|Xs]) when N >= X ->
    [X|insert(N, Xs)];
insert(N, Xs) ->
    [N|Xs].

%% Find all unique permutations of a list.
-spec perms([T]) -> [[T]].
perms(L) ->
    nub(perms(L, length(L), [L])).
perms(_, 0, Acc) ->
    Acc;
perms([X|Xs], N, Acc) ->
    perms(Xs ++ [X], N - 1, append_each(X, perms(Xs)) ++ Acc).

%% Append each sub-list of a nested list with a value.
-spec append_each(T, [[T]]) -> [[T]].
append_each(X, L) ->
    append_each(X, L, []).
append_each(_, [], Acc) ->
    Acc;
append_each(X, [L|Ls], Acc) ->
    append_each(X, Ls, [[X|L]|Acc]).

%% Find set from a list.
-spec nub([T]) -> [T].
nub([]) ->
    [];
nub([X|Xs]) ->
    case member(X, Xs) of
        true ->
            nub(Xs);
        false ->
            [X|nub(Xs)]
    end.

%% ---- Testing Support Functions ----

%% Generate a list of specified length of numbers from 1 to N.
-spec rand_list(pos_integer(), 0) -> [];
               (pos_integer(), non_neg_integer()) -> [pos_integer()].
rand_list(Max, N) ->
    rand_list(Max, N, []).
rand_list(_, 0, Acc) ->
    Acc;
rand_list(Max, N, Acc) ->
    rand_list(Max, N - 1, [rand:uniform(Max)|Acc]).

%% Check that a list is sorted.
-spec check_sorted(list()) -> true | false.
check_sorted([X, Y|Rest]) ->
    if X =< Y ->
            check_sorted([Y|Rest]);
       true ->
            io:format("~B not lt ~B~n", [X,Y]),
            false
    end;
check_sorted(_) ->
    true.

rand_list_test() ->
    ?assertEqual([], rand_list(1, 0)),
    ?assertMatch([_, _], rand_list(1, 2)).

check_sorted_test() ->
    ?assert(check_sorted([])),
    ?assert(check_sorted([0])),
    ?assert(check_sorted([0, 1, 2, 3])),
    ?assertNot(check_sorted([1, 0])),
    ?assertNot(check_sorted([0, 1, 0])).

%% ---- Testing ----

join_test() ->
    ?assertEqual([], join([], [])),
    ?assertEqual([0], join([0], [])),
    ?assertEqual([0], join([], [0])),
    ?assertEqual([0, 1], join([0], [1])),
    ?assertEqual([0, 1, 2, 3], join([0, 1], [2, 3])).

concat_test() ->
    ?assertEqual([], concat([])),
    ?assertEqual([], concat([[], []])),
    ?assertEqual([0, 1], concat([[0], [1]])),
    ?assertEqual([0, 1, 2, 3], concat([[0], [1, 2], [3]])).

member_test() ->
    ?assertNot(member(0, [])),
    ?assertNot(member(0, [1])),
    ?assert(member(0, [0])),
    ?assert(member(1, [0, 1])).

td_merge_sort_test() ->
    ?assertEqual([], td_merge_sort([])),
    ?assertEqual([0], td_merge_sort([0])),
    ?assertEqual([0, 1, 2, 3], td_merge_sort([0, 1, 2, 3])),
    ?assertEqual([0, 1, 2, 3], td_merge_sort([3, 2, 1, 0])),
    ?assert(check_sorted(td_merge_sort(rand_list(1000, 1000)))).

td_split_test() ->
    ?assertEqual({[], []}, td_split([])),
    ?assertEqual({[], [0]}, td_split([0])),
    ?assertEqual({[0], [1]}, td_split([0, 1])),
    ?assertEqual({[0], [2, 1]}, td_split([0, 1, 2])).

td_merge_test() ->
    ?assertEqual([], td_merge([], [])),
    ?assertEqual([0], td_merge([], [0])),
    ?assertEqual([0], td_merge([0], [])),
    ?assertEqual([0, 1, 2, 3], td_merge([0, 3], [1, 2])).

bu_merge_sort_test() ->
    ?assertEqual([], bu_merge_sort([])),
    ?assertEqual([0], bu_merge_sort([0])),
    ?assertEqual([0, 1, 2, 3], bu_merge_sort([0, 1, 2, 3])),
    ?assertEqual([0, 1, 2, 3], bu_merge_sort([3, 2, 1, 0])),
    ?assert(check_sorted(bu_merge_sort(rand_list(1000, 1000)))).

bu_split_test() ->
    ?assertEqual([], bu_split([])),
    ?assertEqual([[0]], bu_split([0])),
    ?assertEqual([[2], [1], [0]], bu_split([0, 1, 2])).

bu_merge_test() ->
    ?assertEqual([], bu_merge([], [])),
    ?assertEqual([0], bu_merge([], [0])),
    ?assertEqual([0], bu_merge([0], [])),
    ?assertEqual([0, 1, 2, 3], bu_merge([0, 3], [1, 2])).

quick_sort_test() ->
    ?assertEqual([], quick_sort([])),
    ?assertEqual([0], quick_sort([0])),
    ?assertEqual([0, 1, 2, 3], quick_sort([0, 1, 2, 3])),
    ?assertEqual([0, 1, 2, 3], quick_sort([3, 2, 1, 0])),
    ?assert(check_sorted(quick_sort(rand_list(1000, 1000)))).

partition_test() ->
    ?assertEqual({[], [], []}, partition(0, [])),
    ?assertEqual({[0, 0], [], []}, partition(1, [0, 0])),
    ?assertEqual({[], [0, 0], []}, partition(0, [0, 0])),
    ?assertEqual({[], [], [0, 0]}, partition(-1, [0, 0])),
    ?assertEqual({[-1], [0], [1]}, partition(0, [1, -1, 0])).

insertion_sort_test() ->
    ?assertEqual([], insertion_sort([])),
    ?assertEqual([0], insertion_sort([0])),
    ?assertEqual([0, 1, 2, 3], insertion_sort([0, 1, 2, 3])),
    ?assertEqual([0, 1, 2, 3], insertion_sort([3, 2, 1, 0])),
    ?assert(check_sorted(insertion_sort(rand_list(1000, 1000)))).

insert_test() ->
    ?assertEqual([0], insert(0, [])),
    ?assertEqual([0, 1, 2], insert(0, [1, 2])),
    ?assertEqual([0, 1, 2], insert(1, [0, 2])),
    ?assertEqual([0, 1, 2], insert(2, [0, 1])).

perms_test() ->
    ?assertEqual([[]], perms([])),
    ?assertEqual([[0]], perms([0])),
    ?assertEqual([[1, 0], [0, 1]], perms([0, 1])),
    ?assertEqual([[3, 1, 2], [3, 2, 1], [2, 3, 1],
                  [2, 1, 3], [1, 3, 2], [1, 2, 3]], perms([1, 2, 3])).

append_each_test() ->
    ?assertEqual([], append_each(0, [])),
    ?assertEqual([[0]], append_each(0, [[]])),
    ?assertEqual([[0, 2], [0, 1]], append_each(0, [[1], [2]])).

nub_test() ->
    ?assertEqual([], nub([])),
    ?assertEqual([0], nub([0])),
    ?assertEqual([0], nub([0, 0])),
    ?assertEqual([1, 0, 2], nub([0, 1, 0, 2])).
